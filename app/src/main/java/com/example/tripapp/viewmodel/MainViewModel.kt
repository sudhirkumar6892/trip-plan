package com.example.tripapp.viewmodel

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModel
import com.example.tripapp.R
import com.example.tripapp.service.LocationUpdateService
import com.example.tripapp.utils.SharedPreference
import com.example.tripapp.utils.Util
import com.google.gson.Gson
import org.json.JSONObject


class MainViewModel : ViewModel() {

   fun onClick(view : View){

      when(view.id)
      {
          R.id.btnStart -> {

              val serviceIntent= Intent(view.context, LocationUpdateService::class.java)
              serviceIntent.putExtra("start_time",Util.getCurrentDate())
              serviceIntent.putExtra("type","start")
              view.context.startService(serviceIntent)

          }

          R.id.btnEnd ->
          {
              val serviceIntent= Intent(view.context, LocationUpdateService::class.java)
              serviceIntent.putExtra("end_time",Util.getCurrentDate())
              serviceIntent.putExtra("type","end")
              view.context.stopService(serviceIntent)
          }

          R.id.btnOutput ->
          {
              val gson = Gson()
              val jsonTut: String = gson.toJson(SharedPreference(view.context).getTripModel())
              if(SharedPreference(view.context).getTripModel().start_time != "" && SharedPreference(view.context).getTripModel().end_time !="") {
                  dataOutput(jsonTut, view.context)
              }
              else
                  Toast.makeText(view.context,"Please Start Trip",Toast.LENGTH_SHORT).show()
          }
      }

    }


    fun dataOutput( data : String,context: Context){


        val builder = AlertDialog.Builder(context)
        builder.setTitle("Output")
        builder.setMessage(data)

        builder.setPositiveButton("Ok") { dialog, which ->

            dialog.dismiss()

        }


        builder.show()


    }


}