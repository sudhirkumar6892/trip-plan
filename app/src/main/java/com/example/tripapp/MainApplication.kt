package com.example.tripapp

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build


class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        createNotification()


    }

    companion object {
        lateinit var instance: MainApplication
            private set
        val context: Context
            get() = instance

        const val CHANNEL_ID = "exampleService"
    }

    private fun createNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                CHANNEL_ID,
                " Example Service",
                NotificationManager.IMPORTANCE_HIGH
            )
            val notificationManager = getSystemService(
                NotificationManager::class.java
            )
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }
}