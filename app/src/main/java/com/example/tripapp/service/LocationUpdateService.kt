package com.example.tripapp.service

import android.Manifest
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import com.example.tripapp.MainActivity
import com.example.tripapp.MainApplication
import com.example.tripapp.MainApplication.Companion.CHANNEL_ID
import com.example.tripapp.R
import com.example.tripapp.model.Trip
import com.example.tripapp.model.TripData
import com.example.tripapp.utils.SharedPreference
import com.example.tripapp.utils.Util
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import java.util.*
import kotlin.collections.ArrayList


class LocationUpdateService : Service() {

    private val mHandler = Handler(Looper.getMainLooper()) //run on another Thread to avoid crash
    private var mTimer: Timer? = null //timer handling
    var location: Location? = null
    var tripData: ArrayList<TripData>? = ArrayList()
    var trip: Trip? = null
    var start_time: String? = ""
    var end_time: String? = ""

    override fun onBind(intent: Intent): IBinder? {
        throw UnsupportedOperationException("Not yet implemented")
    }

    override fun onCreate() {
        if (mTimer != null) // Cancel if already existed
            mTimer!!.cancel() else mTimer = Timer() //recreate new
        mTimer?.scheduleAtFixedRate(TimeDisplay(), 0, notify) //Schedule task
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        getLocation(MainApplication.context)
        if(intent!= null) {

            if (intent.extras?.get("type")!!.equals("start")) {
                start_time = intent.extras?.get("start_time").toString()
            }else if(intent.extras?.get("type")!!.equals("end")){
                end_time = intent.extras?.get("end_time").toString()

            }
             trip = Trip("1",start_time!!,end_time!!,tripData!!)


        }

        val intent1 = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent1, 0)
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("App is Running")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(pendingIntent)
            .build()
        startForeground(1, notification)
        return START_NOT_STICKY
    }


    override fun onDestroy() {
        super.onDestroy()
        trip?.end_time =Util.getCurrentDate()
        SharedPreference(applicationContext).saveTripModel(trip!!)
        mTimer!!.cancel() //For Cancel Timer
    }

    //class TimeDisplay for handling task
    internal inner class TimeDisplay : TimerTask() {
        override fun run() {
            // run on another thread
            mHandler.post {
                // MainApplication.context.showToast("" + location)

                if (location != null && location?.latitude!! > 0 && location?.longitude!! > 0) {
                    Log.e("location",""+location)
                    tripData!!.add(TripData(""+location?.latitude,""+location?.latitude,Util.getCurrentDate(),""+location?.accuracy))

                }
            }


        }
    }


    companion object {
        const val notify: Long =
            5000 //interval between two services(Here Service run every 5 sec)
    }

    private val mLocationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            location = locationResult.lastLocation

        }
    }

    fun getLocation(context: Context) {
        val mLocationRequest = LocationRequest()
        mLocationRequest.interval = 5000
        mLocationRequest.fastestInterval = 5000
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details
        }

        LocationServices.getFusedLocationProviderClient(context).requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.getMainLooper()
        )


    }


}