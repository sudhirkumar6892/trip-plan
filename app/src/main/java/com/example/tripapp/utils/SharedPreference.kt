package com.example.tripapp.utils

import android.content.Context
import android.content.SharedPreferences
import com.example.tripapp.model.Trip
import com.example.tripapp.model.TripData
import com.google.gson.Gson


class SharedPreference(_context: Context) {

    private var pref: SharedPreferences = _context.getSharedPreferences(Constants.PREF_NAME, 0)
    private var editor: SharedPreferences.Editor = pref.edit()

    init {
        editor.apply()
    }


    fun saveTripModel(model: Trip) {
        try {
            val gson = Gson()
            val json = gson.toJson(model)
            editor.putString(Constants.TRIPDATA, json)
            editor.commit()
        }catch (ee:java.lang.Exception){}
    }

    fun getTripModel(): Trip {
        var obj: Trip? = Trip("", "","", ArrayList<TripData>())
        try {
            val gson = Gson()
            val json: String? = pref.getString(Constants.TRIPDATA, obj.toString())
            obj = gson.fromJson(json, Trip::class.java)
        }catch (ee:Exception){}
        return obj!!
    }

}