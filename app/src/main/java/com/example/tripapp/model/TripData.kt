package com.example.tripapp.model

data class TripData(val latitude : String,val longitude : String ,val timestamp : String ,val accuracy : String)