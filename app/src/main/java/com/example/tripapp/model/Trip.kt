package com.example.tripapp.model


data class Trip(val trip_id : String, val start_time : String, var end_time : String, val locations : ArrayList<TripData>)

